-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 22, 2019 at 09:01 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_radya`
--
CREATE DATABASE IF NOT EXISTS `db_radya` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `db_radya`;

-- --------------------------------------------------------

--
-- Table structure for table `p_home`
--

CREATE TABLE `p_home` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `langitude` decimal(11,8) DEFAULT NULL,
  `latitude` decimal(10,8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_home`
--

INSERT INTO `p_home` (`id`, `name`, `type`, `address`, `langitude`, `latitude`) VALUES
(1, 'WR Supratman', 32, '98, Jalan W. R. Supratman, Bandung Kota 40122, Indonesia', '107.62705859', '-6.90219480'),
(3, 'Test 1 Edit', 32, 'Jalan Lembong, Bandung Kota 40111, Indonesia', '107.60995371', '-6.91648386'),
(4, 'Dago 174', 72, '174, Jalan Ir. H. Juanda, Bandung Kota 40132, Indonesia', '107.61377837', '-6.88486051');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `p_home`
--
ALTER TABLE `p_home`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `p_home`
--
ALTER TABLE `p_home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
