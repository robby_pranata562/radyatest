﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestRadya.Models;
using TestRadya.DB;
namespace TestRadya.Class
{
    public class HouseClass
    {
        db_radyaEntities entities;
        private List<HouseModel> _modelHome = new List<HouseModel>();
        private List<RuteModel> _modelRute = new List<RuteModel>();
        public List<HouseModel> GetAllHome()
        {
            entities = new db_radyaEntities();
            var ListHome = (from p in entities.p_home
                            select new HouseModel
                                      {
                                          id = p.id,
                                          name = p.name,
                                          type = p.type,
                                          address = p.address,
                                          langitude = p.langitude.ToString(),
                                          latitude = p.latitude.ToString()
                                      }).ToList();
            return ListHome;
        }

        public HouseModel GetAllHomeByID(int id)
        {
            entities = new db_radyaEntities();
            var ListHome = (from p in entities.p_home.Where(x=>x.id == id)
                            select new HouseModel
                            {
                                id = p.id,
                                name = p.name,
                                type = p.type,
                                address = p.address,
                                langitude = p.langitude.ToString(),
                                latitude = p.latitude.ToString()
                            }).FirstOrDefault();
            return ListHome;
        }

        public int AddHome(HouseModel model)
        {
            entities = new db_radyaEntities();
            var home = new p_home();
            home.name = model.name;
            home.type = model.type;
            home.address = model.address;
            home.langitude = Convert.ToDecimal(model.langitude);
            home.latitude = Convert.ToDecimal(model.latitude);
            entities.p_home.Add(home);
            return entities.SaveChanges();
        }

        public int EditHome(HouseModel model)
        {
            entities = new db_radyaEntities();
            var home = entities.p_home.Where(x => x.id == model.id).FirstOrDefault();
            home.name = model.name;
            home.type = model.type;
            home.address = model.address;
            home.langitude = Convert.ToDecimal(model.langitude);
            home.latitude = Convert.ToDecimal(model.latitude);
            return entities.SaveChanges();
        }

        public int DeleteHome(int ID)
        {
            entities = new db_radyaEntities();
            var home = entities.p_home.Where(x => x.id == ID).FirstOrDefault();
            entities.p_home.Remove(home);
            return entities.SaveChanges();
        }

        public List<RuteModel> FindDistanceBetween(string _latitude ,string _longitude , string _dest)
        {
            var initial_location = new RuteModel();
            initial_location.Description = "Your Location";
            initial_location.Latitude = _latitude;
            initial_location.Longitude = _longitude;
            initial_location.Distance = "0";
            _modelRute.Add(initial_location);
            foreach (var item in _dest.Split(','))
            {
                  entities = new db_radyaEntities();
                  var _item = item.TrimStart().TrimEnd();
                 
                  var Dest = (from p in entities.p_home.Where(x => x.name.Contains(_item))
                            select new RuteModel
                            {
                                Description = p.name,
                                Longitude = p.langitude.ToString(),
                                Latitude = p.latitude.ToString(),
                                Distance = ""
                            }).FirstOrDefault();
                  _modelRute.Add(Dest);
            }
            for (int i = 0; i < _modelRute.Count; i++)
            {
                if (i + 1 == _modelRute.Count)
                    break;
                _modelRute[i].Distance = calculate(
                    Convert.ToDouble(_modelRute[i].Latitude),
                    Convert.ToDouble(_modelRute[i].Longitude),
                    Convert.ToDouble(_modelRute[i + 1].Latitude),
                    Convert.ToDouble(_modelRute[i + 1].Longitude)).ToString();
            }
            return _modelRute;
        }

        public static double calculate(double lat1, double lon1, double lat2, double lon2)
        {
            var R = 6372.8; // In kilometers
            var dLat = toRadians(lat2 - lat1);
            var dLon = toRadians(lon2 - lon1);
            lat1 = toRadians(lat1);
            lat2 = toRadians(lat2);

            var a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) + Math.Sin(dLon / 2) * Math.Sin(dLon / 2) * Math.Cos(lat1) * Math.Cos(lat2);
            var c = 2 * Math.Asin(Math.Sqrt(a));
            return R * 2 * Math.Asin(Math.Sqrt(a));
        }

        public static double toRadians(double angle)
        {
            return Math.PI * angle / 180.0;
        }
    }
}