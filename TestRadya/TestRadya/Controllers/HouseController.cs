﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestRadya.Class;
using TestRadya.Models;
namespace TestRadya.Controllers
{
    public class HouseController : Controller
    {


        // GET: House
        HouseClass _houseClass = new HouseClass();
        HouseModel _houseModel = new HouseModel();
       
        public ActionResult Index()
        {
            List<HouseModel> _modelHouse = new List<HouseModel>();
            _modelHouse = _houseClass.GetAllHome();
            return View(_modelHouse);
        }

        // GET: House/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: House/Create
        public ActionResult Create()
        {
            var ListTypeHouse = new SelectList(new[]
                                        {

                                                new { ID = "36", Name = "36" },
                                                new { ID = "45", Name = "45" },
                                                new { ID = "72", Name = "72" },
                                        }, "ID", "Name");
            ViewData["TH"] = ListTypeHouse;
            return View();
        }

        // POST: House/Create
        [HttpPost]
        public ActionResult Create(HouseModel model)
        {
            try
            {
                var res = _houseClass.AddHome(model);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: House/Edit/5
        public ActionResult Edit(int id)
        {
            var res = _houseClass.GetAllHomeByID(id);
            var ListTypeHouse = new SelectList(new[]
                                        {

                                                new { ID = "36", Name = "36" },
                                                new { ID = "45", Name = "45" },
                                                new { ID = "72", Name = "72" },
                                        }, "ID", "Name",res.type);
            ViewData["TH"] = ListTypeHouse;

            return View(res);
        }

        // POST: House/Edit/5
        [HttpPost]
        public ActionResult Edit(HouseModel model)
        {
            try
            {
                var res = _houseClass.EditHome(model);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: House/Delete/5
        public ActionResult Delete(int id)
        {
            var res = _houseClass.GetAllHomeByID(id);
            return View(res);
        }

        // POST: House/Delete/5
        [HttpPost]
        public ActionResult Delete(HouseModel model)
        {
            try
            {
                // TODO: Add delete logic here
                var res = _houseClass.DeleteHome(model.id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
