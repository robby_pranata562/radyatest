﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestRadya.Class;
using TestRadya.Models;
using Newtonsoft.Json;
namespace TestRadya.Controllers
{
    public class RuteController : Controller
    {
        // GET: Rute
        HouseClass _houseClass = new HouseClass();
        HouseModel _houseModel = new HouseModel();
        public ActionResult Index()
        {
            List<HouseModel> _modelHouse = new List<HouseModel>();
            _modelHouse = _houseClass.GetAllHome();
            return View(_modelHouse);
        }

        public ActionResult FindDistance(string Latitude, string Longitude, string Dest)
        {
            var distance = _houseClass.FindDistanceBetween(Latitude, Longitude, Dest);
            return Json(new { data = distance });
        }

        // GET: Rute/Details/5
       
    }
}
