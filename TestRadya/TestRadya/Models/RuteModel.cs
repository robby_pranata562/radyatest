﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestRadya.Models
{
    public class RuteModel
    {
        public string Description { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public string Distance { get; set; }

    }
}