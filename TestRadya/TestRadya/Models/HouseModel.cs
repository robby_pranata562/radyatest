﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestRadya.Models
{
    public class HouseModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public Nullable<int> type { get; set; }
        public string address { get; set; }
        public string langitude { get; set; }
        public string latitude { get; set; }
    }
}